require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get cake_shop" do
    get categories_cake_shop_url
    assert_response :success
  end

  test "should get drinks" do
    get categories_drinks_url
    assert_response :success
  end

  test "should get candy_store" do
    get categories_candy_store_url
    assert_response :success
  end

end
