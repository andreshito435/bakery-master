class NotificationMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def welcome_email(contact)
    @contact = contact
    mail(to: "andreshito435@gmail.com", subject: 'Alguien desea comunicarse con tigo.')
  end
end
