Rails.application.routes.draw do

  resources :products
  get 'clientes', to: 'admin_page#clientes'
  get 'new', to: 'products#new'
  get 'cake_shop', to: 'categories#cake_shop'
  get 'drinks', to:'categories#drinks'
  get 'candy_store', to: 'categories#candy_store'
  get 'admin', to: 'admin_page#index'
  devise_for :users, :controllers => { registrations: 'registrations' }
  # root 'static_pages#home'
  post "create" => 'static_pages#create'
  root to: "static_pages#home"
  get 'help' => 'static_pages#help'
  get 'products' => 'static_pages#products'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
