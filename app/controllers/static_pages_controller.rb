class StaticPagesController < ApplicationController
  def home
    @contact = Contact.new
  end

  def create
    @contact= Contact.new(contact_params)
    if @contact.save
      NotificationMailer.welcome_email(@contact).deliver_now!
      redirect_to root_path, notice: 'Mensaje enviado'
    else
      render:home
    end
  end

  def help
  end

  def products
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :reason, :message)
  end
end
