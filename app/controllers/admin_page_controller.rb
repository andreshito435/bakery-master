class AdminPageController < ApplicationController
  def index
  end

  def clientes
    @users = User.all.order("created_at")  
  end
end
